This is an archive of Steve Kemp's `milli` bug tracker. 

It's a tiny, distributed, bug tracker that is perfect for small
projects.  This was once hosted on Steve's website, but apparently he
switched from being "steve kemp in the uk" (steve.org.uk) to being
"steve kemp in finland" (steve.kemp.fi) and certain things fell through
the cracks. 

At any rate, I couldn't find it when I asked the Duck for a couple of
months, and then one day I stumbled on `be` which led to `b` which led
to `t` (or vice versa) which put me on the path of `milli`. And now here
we are!

We'll see how long it takes for this one to get lost. Hopefully,
somebody in 2030 will be able to find this and recover it. 👍
